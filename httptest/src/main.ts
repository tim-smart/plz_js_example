import { createServer } from "http";
import { superfn } from "superfn";

const server = createServer((req, res) => {
  superfn();
  res.writeHead(200);
  res.end("httptest");
});

server.listen(process.env.PORT || 3000);
