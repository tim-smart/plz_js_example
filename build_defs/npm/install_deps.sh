#!/usr/bin/env bash

if [ -n "$SRCS_DEPS" ]; then
	echo ./${SRCS_DEPS// /./} \
		| xargs $TOOLS_NPM install --production --no-bin-links
fi
